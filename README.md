# PPK-Praktikum 14 : Latihan SQLite

# Identitas

```
Nama : Maulana Pandudinata
NIM  : 222011379
Kelas: 3SI3

```

# Deskripsi

SQLite merupakan sebuah sistem manajemen basisdata relasional yang bersifat ACID-compliant dan memiliki ukuran pustaka kode yang relatif kecil, ditulis dalam bahasa C. 
SQLite merupakan jenis database yang ringan dan tersedia dalam OS Android. 
Pada praktikum kali ini kita akan mempraktekkan android storage menggunakan SQLite, lengkap dengan proses CRUD dan menerapkan RecyclerView yang sudah dipelajari sebelumnya. 

# 1. Tambah Data Mahasiswa
## a. Input Data Mahasiswa
![cd_catalog.xml](https://gitlab.com/222011379/ppk-praktikum14/-/raw/main/Screenshot/TambahData%20(1).png)
## b. Tambah Data Mahasiswa Berhasil
![cd_catalog.xml](https://gitlab.com/222011379/ppk-praktikum14/-/raw/main/Screenshot/TambahData%20(2).png)

# 2. Lihat Hasil Entri Data Mahasiswa 
## a. Tampilan Menu
![cd_catalog.xml](https://gitlab.com/222011379/ppk-praktikum14/-/raw/main/Screenshot/LihatData%20(1).png)
## b. Menu Lihat Data Mahasiswa
![cd_catalog.xml](https://gitlab.com/222011379/ppk-praktikum14/-/raw/main/Screenshot/LihatData%20(2).png)

# 3. Update Data Mahasiswa 
## a. Data Sebelum Update
![cd_catalog.xml](https://gitlab.com/222011379/ppk-praktikum14/-/raw/main/Screenshot/UpdateData%20(1).png)
![cd_catalog.xml](https://gitlab.com/222011379/ppk-praktikum14/-/raw/main/Screenshot/UpdateData%20(2).png)
## b. Input Data Mahasiswa Update
![cd_catalog.xml](https://gitlab.com/222011379/ppk-praktikum14/-/raw/main/Screenshot/UpdateData%20(3).png)
![cd_catalog.xml](https://gitlab.com/222011379/ppk-praktikum14/-/raw/main/Screenshot/UpdateData%20(4).png)
## c. Data Setelah Update
![cd_catalog.xml](https://gitlab.com/222011379/ppk-praktikum14/-/raw/main/Screenshot/UpdateData%20(5).png)

# 4. Hapus Data Mahasiswa 
## a. Data Sebelum Dihapus
![cd_catalog.xml](https://gitlab.com/222011379/ppk-praktikum14/-/raw/main/Screenshot/DeleteData%20(1).png)
## b. Data Mahasiswa Dihapus
![cd_catalog.xml](https://gitlab.com/222011379/ppk-praktikum14/-/raw/main/Screenshot/DeleteData%20(2).png)
![cd_catalog.xml](https://gitlab.com/222011379/ppk-praktikum14/-/raw/main/Screenshot/DeleteData%20(3).png)
## c. Data Setelah Dihapus
![cd_catalog.xml](https://gitlab.com/222011379/ppk-praktikum14/-/raw/main/Screenshot/DeleteData%20(4).png)
